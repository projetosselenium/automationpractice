package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Pagamento extends Main {
    public Pagamento(WebDriver navegador) {
        super(navegador);
    }

    public Pagamento AcionarProceedCheckout() {
        navegador.findElement(By.xpath("//div[@id='center_column']/p[2]/a/span")).click();
        return this;
    }

    public Login InformarEmailLogin(String email) {
        navegador.findElement(By.id("email")).sendKeys(email);
        return new Login(navegador);
    }

    public Pagamento SelecionarCartao() {
        navegador.findElement(By.linkText("Pay by bank wire (order processing will be longer)")).click();
        return this;
    }

    public Pagamento ConfirmarPagamento() {
        navegador.findElement(By.xpath("//p[@id='cart_navigation']/button/span")).click();
        return this;
    }
    public String ConfirmarMensagem(){
        WebElement mensagem = navegador.findElement(By.xpath
                ("//strong[text()=\"Your order on My Store is complete.\"]"));
        System.out.println(mensagem);
        return mensagem.getText();

    }
}