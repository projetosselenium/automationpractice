package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SingUp extends Main {
    public SingUp(WebDriver navegador) {
        super(navegador);
    }

    public SingUp InformarEmail(String email) {
        navegador.findElement(By.id("email_create")).sendKeys(email);
        return this;
    }

    public CreateConta AcionarCreate() {
        navegador.findElement(By.xpath("//button[@id='SubmitCreate']/span")).click();
        return new CreateConta(navegador);

    }
    

}