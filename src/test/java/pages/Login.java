package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login extends Main {
    public Login(WebDriver navegador) {
        super(navegador);
    }

    public Login InformarPassword(String password) {
        navegador.findElement(By.id("passwd")).sendKeys(password);
        return this;
    }

    public ConfirmarAddress AcionarSingIn(){
        navegador.findElement(By.xpath("//button[@id='SubmitLogin']/span")).click();
        return new ConfirmarAddress(navegador);
    }
}
