package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Index extends Main {
    public Index(WebDriver navegador) {
        super(navegador);
    }

    public SingUp CreateAcoount() {
        navegador.findElement(By.linkText("Sign in")).click();
        return new SingUp(navegador);
    }

    public PesquisarComprar InformarSearch(String search) {
        navegador.findElement
                (By.id("search_query_top")).sendKeys(search);
        return new PesquisarComprar(navegador);
    }
}