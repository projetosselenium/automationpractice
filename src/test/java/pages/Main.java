package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Main {
    protected WebDriver navegador;

    public Main(WebDriver navegador) {
        this.navegador = navegador;
    }

    public String MensagemErro() {
        WebElement mensagemErro = navegador.findElement(By.xpath
                ("//div[@id='create_account_error']/ol/li"));
        return mensagemErro.getText();

    }
}