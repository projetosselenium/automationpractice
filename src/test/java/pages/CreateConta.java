package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateConta extends Main {
    public CreateConta(WebDriver navegador) {
        super(navegador);

    }

    public CreateConta InformarTitle() {
        navegador.findElement(By.cssSelector("label[for=\"id_gender1\"]")).click();
        return this;

    }

    public CreateConta InformarFirstName(String nome) {
        navegador.findElement(By.id("customer_firstname")).sendKeys(nome);
        return this;
    }

    public CreateConta InformarLastName(String nome) {
        navegador.findElement(By.id("customer_lastname")).sendKeys(nome);
        return this;

    }

    public CreateConta InformarPassword(String password) {
        navegador.findElement(By.id("passwd")).sendKeys(password);
        return this;

    }

    public CreateConta EscolherDaysBirth() {
        navegador.findElement(By.id("days")).click();
        new Select(navegador.findElement(By.xpath("//select[@id=\"days\"]")))
                .selectByValue("2");
        navegador.findElement(By.id("days")).click();
        return this;
    }

    public CreateConta EscolherMonthsBirth() {
        navegador.findElement(By.id("months")).click();
        new Select(navegador.findElement(By.xpath("//select[@id=\"months\"]")))
                .selectByValue("2");
        navegador.findElement(By.id("months")).click();
        return this;

    }

    public CreateConta EscolherYearsBirth() {
        navegador.findElement(By.id("years")).click();
        new Select(navegador.findElement(By.xpath("//select[@id=\"years\"]")))
                .selectByValue("1995");
        navegador.findElement(By.id("months")).click();
        return this;
    }

    /*public CreateAccout EscolherBirt(){

        navegador.findElement(By.id("days")).click();
        new Select(navegador.findElement(By.xpath("//select[@id=\"days\"]")))
                .selectByValue("2");
        navegador.findElement(By.id("days")).click();

        navegador.findElement(By.id("months")).click();
        new Select(navegador.findElement(By.xpath("//select[@id=\"months\"]")))
                .selectByValue("2");
        navegador.findElement(By.id("months")).click();

        navegador.findElement(By.id("years")).click();
        new Select(navegador.findElement(By.xpath("//select[@id=\"years\"]")))
                .selectByValue("1995");
        navegador.findElement(By.id("months")).click();
        return this;
    }*/

    public CreateConta SelecionarAssinarNewletter() {
        navegador.findElement(By.id("newsletter")).click();
        return this;

    }

    public CreateConta SelecionarReceiveSpecial() {
        navegador.findElement(By.id("optin")).click();
        return this;

    }

    public CreateConta InformarAddressFirstName() {
        navegador.findElement(By.id("firstname")).clear();
        navegador.findElement(By.id("firstname")).sendKeys("Teste2656");
        return this;
    }

    public CreateConta InformarAddressLastName() {
        navegador.findElement(By.id("lastname")).clear();
        navegador.findElement(By.id("lastname")).sendKeys("Selenium");
        return this;
    }

    public CreateConta InformarAddressCompany(String company) {
        navegador.findElement(By.id("company")).sendKeys(company);
        return this;
    }

    public CreateConta InformarAddress(String address) {
        navegador.findElement(By.id("address1")).sendKeys(address);
        return this;
    }

    public CreateConta InformarAddressCity(String city) {
        navegador.findElement(By.id("city")).sendKeys(city);
        return this;
    }

    public CreateConta InformarEscolherState(String state) {
        navegador.findElement(By.id("id_state")).click();
        new Select(navegador.findElement(By.id("id_state"))).selectByVisibleText(state);
        navegador.findElement(By.id("id_state")).click();
        return this;
    }

    public CreateConta InformarAddressPostal(String postal) {
        navegador.findElement(By.id("postcode")).sendKeys(postal);
        return this;
    }

    public CreateConta InformarEscolherCountry(String country) {
        navegador.findElement(By.id("id_country")).click();
        new Select(navegador.findElement(By.id("id_country"))).selectByVisibleText(country);
        navegador.findElement(By.id("id_country")).click();
        return this;
    }
    public CreateConta InformarAdditionalInformation(String information) {
        navegador.findElement(By.id("other")).sendKeys(information);
        return this;
    }
    public CreateConta InformarHomePhone(String phone) {
        navegador.findElement(By.id("phone")).sendKeys(phone);
        return this;
    }

    public CreateConta InformarMobilePhone(String mobile) {
        navegador.findElement(By.id("phone_mobile")).sendKeys(mobile);
        return this;
    }

    public CreateConta InformarAssingAndress(String assing) {
        navegador.findElement(By.id("alias")).clear();
        navegador.findElement(By.id("alias")).sendKeys(assing);
        return this;
    }

    public SingUp AcionarRegister() {
        navegador.findElement(By.id("account-creation_form")).submit();
        return new SingUp(navegador);
    }

}