package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class PesquisarComprar extends Main {

    public PesquisarComprar(WebDriver navegador) {
        super(navegador);
    }

    public PesquisarComprar AcionarSearch() {
        navegador.findElement(By.name("submit_search")).click();
        return this;
    }

    public PesquisarComprar AcionarList() {
        navegador.findElement(By.linkText("List")).click();
        //Acionei dois cliques devido estabilidade
        navegador.findElement(By.linkText("List")).click();
        return this;
    }

    public PesquisarComprar SelecionarColor() {
        navegador.findElement(By.id("color_20")).click();
        return this;

    }

    public PesquisarComprar InformarQuantity(String quantity) {
        navegador.findElement(By.id("quantity_wanted")).clear();
        navegador.findElement(By.id("quantity_wanted")).sendKeys(quantity);
        return this;

    }

    public PesquisarComprar SelecionarQuantity() {
        navegador.findElement(By.xpath("//p[@id='quantity_wanted_p']/a[2]/span/i")).click();
        return this;

    }

    public PesquisarComprar SelecionarSize() {
        navegador.findElement(By.id("group_1")).click();
        new Select(navegador.findElement(By.id("group_1"))).selectByVisibleText("M");
        return this;
    }

    public PesquisarComprar SelecionarZomm() {
        navegador.findElement(By.xpath("//span[@id='view_full_size']/span")).click();
        return this;
    }

    public PesquisarComprar SelecionarClose(){
        navegador.findElement(By.xpath("//body[@id='product']/div[2]/div/a")).click();
        return this;
    }
    public AddCard AcionarAddCart() {
        navegador.findElement(By.xpath("//p[@id='add_to_cart']/button/span")).click();
        return new AddCard (navegador);
    }
}