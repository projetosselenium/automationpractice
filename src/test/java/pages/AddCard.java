package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddCard extends Main {
    public AddCard(WebDriver navegador) {
        super(navegador);
    }

    public Pagamento AcionarCheckout(){
        navegador.findElement(By.xpath("//div[@id='layer_cart']/div/div[2]/div[4]/a/span")).click();
        return new Pagamento(navegador);
    }
}
