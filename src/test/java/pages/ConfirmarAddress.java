package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ConfirmarAddress extends Main {
    public ConfirmarAddress(WebDriver navegador) {
        super(navegador);
    }

    public ConfirmarAddress InformarComment(String comment) {
        navegador.findElement(By.name("message")).sendKeys(comment);
        return this;

    }

    public Shipping AcionarProceedCheckoutAndress() {
        navegador.findElement(By.xpath("//div[@id='center_column']/form/p/button/span")).click();
        return new Shipping(navegador);

    }
}