package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Shipping extends Main {
    public Shipping(WebDriver navegador) {
        super(navegador);
    }

    public Shipping AceitaService() {
        navegador.findElement(By.id("cgv")).click();
        return this;
    }

    public Pagamento AcionarProceedCheckoutShipping() {
        navegador.findElement(By.xpath("//form[@id='form']/p/button/span")).click();
        return new Pagamento(navegador);
    }

}