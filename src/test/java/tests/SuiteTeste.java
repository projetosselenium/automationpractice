package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;
import pages.Index;
import suporte.Generator;
import suporte.Screenshot;
import suporte.Web;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SuiteTeste {
    private WebDriver navegador;

    @Rule
    public TestName test = new TestName();

    @Before
    public void setup() {
        navegador = Web.createChorme();
        navegador.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //navegador = Web.createBrowserStack();
    }

    @Test
    public void CriarConta() {
        new Index(navegador)
                .CreateAcoount()
                .InformarEmail("joaosilvateste@gmail.com")
                .AcionarCreate()
                .InformarTitle()
                .InformarFirstName("Joao Da Silva")
                .InformarLastName("Santos")
                .InformarPassword("Teste01")
                .EscolherDaysBirth()
                .EscolherMonthsBirth()
                .EscolherYearsBirth()
                .SelecionarAssinarNewletter()
                .SelecionarReceiveSpecial()
                .InformarAddressFirstName()
                .InformarAddressLastName()
                .InformarAddressCompany("Teste")
                .InformarAddress("Rua Principal")
                .InformarAddressCity("Aurora")
                .InformarEscolherState("Colorado")
                .InformarAddressPostal("00000")
                .InformarEscolherCountry("United States")
                .InformarAdditionalInformation("Teste do United States")
                .InformarHomePhone("+002 5656 65656")
                .InformarMobilePhone("+002 5656 65655265")
                .InformarAssingAndress("Rua das Palneiras")
                .AcionarRegister();

        // Print da tela - Ver como colocar na classe
        String screenshotArquivo = "C:\\Users\\Administrador\\Desktop\\Programacao\\Prints\\"
                + Generator.dataHoraParaArquivo() + test.getMethodName() + ".png";
        Screenshot.tirar(navegador, screenshotArquivo);
    }



    @Test
    public void CriarContaDuplicado() {
        String MensagemErro = new Index(navegador)
                .CreateAcoount()
                .InformarEmail("teste@gmail.com")
                .AcionarCreate()
                .MensagemErro();
        
        assertEquals("An account using this email address has already been registered. Please enter a valid password or request a new one.", MensagemErro);


        // Print da tela - Ver como colocar na classe
        String screenshotArquivo = "C:\\Users\\Administrador\\Desktop\\Programacao\\Prints\\"
                + Generator.dataHoraParaArquivo() + test.getMethodName() + ".png";
        Screenshot.tirar(navegador, screenshotArquivo);
    }

    @Test
    public void CriarContaEmailInvalido() {
        String MensagemErro = new Index(navegador)
                .CreateAcoount()
                .InformarEmail("")
                .AcionarCreate()
                .MensagemErro();

        assertEquals("Invalid email address.", MensagemErro);


        // Print da tela - Ver como colocar na classe
        String screenshotArquivo = "C:\\Users\\Administrador\\Desktop\\Programacao\\Prints\\"
                + Generator.dataHoraParaArquivo() + test.getMethodName() + ".png";
        Screenshot.tirar(navegador, screenshotArquivo);
    }

    @Test
    public void Comprar() {
      String Confirmar =  new Index(navegador)
                .InformarSearch("Printed")
                .AcionarSearch()
                .AcionarList()
                .SelecionarColor()
                .SelecionarQuantity()
                .SelecionarSize()
                //.InformarQuantity("10")
                .SelecionarZomm()
                .SelecionarClose()
                .AcionarAddCart()
                .AcionarCheckout()
                .AcionarProceedCheckout()
                .InformarEmailLogin("murilocarlosfreitas@gmail.com")
                .InformarPassword("Muril@95")
                .AcionarSingIn()
                .InformarComment("Teste de compra")
                .AcionarProceedCheckoutAndress()
                .AceitaService()
                .AcionarProceedCheckoutShipping()
                .SelecionarCartao()
                .ConfirmarPagamento()
                .ConfirmarMensagem();

        assertEquals("Your order on My Store is complete.", Confirmar);

        // Print da tela - Ver como colocar na classe
        String screenshotArquivo = "C:\\Users\\Administrador\\Desktop\\Programacao\\Prints\\"
                + Generator.dataHoraParaArquivo() + test.getMethodName() + ".png";
        Screenshot.tirar(navegador, screenshotArquivo);
    }

    @After
    public void Destruir() {
        //Fechar o navegador
        navegador.quit();
    }
}