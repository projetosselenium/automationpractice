package suporte;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Web {

    public static final String AUTOMATE_USERNAME = "murilocarlos_riYduW";
    public static final String AUTOMATE_ACCESS_KEY = "4G9e8wYexUMsnAzphuxX";
    public static final String URL = "https://" + AUTOMATE_USERNAME + ":" + AUTOMATE_ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";

    public static WebDriver createChorme() {

        //Abrindo o navegador
        System.setProperty("webdriver.chorme.driver",
                "C:\\Program Files (x86)\\Common Files\\Oracle\\Java\\javapath\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Navegando para a pagína do AutomationPractice
        navegador.get("https://www.caixa.gov.br/Paginas/home-caixa.aspx");

        return navegador;
    }

    public static WebDriver createBrowserStack() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "90.0");

        WebDriver navegador = null;

        try {
            navegador = new RemoteWebDriver(new URL(URL), caps);

            navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            // Navegando para a pagína do AutomationPractice
            navegador.get("http://automationpractice.com/index.php");

        } catch (MalformedURLException e) {
            System.out.println("Houveram problemas com a URL" + e.getMessage());
        }
        return navegador;
    }
}